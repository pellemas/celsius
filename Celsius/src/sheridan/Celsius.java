package sheridan;

/*
 * @author Mason Pelletier 991524171
 * 
 * This class converts celsius to farenheit
 */

public class Celsius {
	
	
public static int fromFarenheit (int value) {
		
		int newValue = (int)(( 5 *(value - 32.0)) / 9.0);
		
		return newValue;
	}

}
