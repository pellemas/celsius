package sheridan;

import static org.junit.Assert.*;

/*
 * @author Mason Pelletier 991524171
 * 
 * This class validates the farenheit to celsius conversion and it will be developed using TDD
 */

import org.junit.Test;

public class CelsiusTest {

	@Test
	public void testConvertToCelsius() {
		assertEquals("Wrong value", 4,  Celsius.fromFarenheit(40));
	}
	@Test
	public void testConvertToCelsiusException() {
		assertEquals("Wrong value", 1,  Celsius.fromFarenheit((int)35.9));
	}
	@Test
	public void testConvertToCelsiusBoundaryIn() {
		assertEquals("Wrong value", -15,  Celsius.fromFarenheit(5));
	}
	@Test
	public void testConvertToCelsiusBoundaryOut() {
		assertEquals("Wrong value", -34,  Celsius.fromFarenheit(-30));
	}

}
